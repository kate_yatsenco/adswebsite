<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * AdRepository
 */
class AdRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAdsQuery(): ?Query
    {
        return $this->getEntityManager()
            ->createQuery('Select a From AppBundle:Ad a');
    }
}
