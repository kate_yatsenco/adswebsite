<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Ad;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class WebsiteFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User;
        $password = $this->passwordEncoder->encodePassword($user, 'test');
        $user->setUsername('test_user')
            ->setPassword($password);
        $manager->persist($user);

        for ($i = 0; $i < 10; $i++) {
            $ad = new Ad();
            $ad->setTitle('title_' . $i)
                ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id 
            est laborum.')
                ->setUser($user)
                ->setAuthorName($user->getUsername())
            ->setCreatedAt(new \DateTime('-'.rand(1, 9).'day'));
            $user->addAd($ad);
            $manager->persist($user);
            $manager->persist($ad);
        }
        $manager->flush();
    }
}
