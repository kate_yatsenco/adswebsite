<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Form\Type\AdType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AdController extends Controller
{
    const AD_COUNT_PER_PAGE = 5;

    /**
     * @Route("/", name="show_ads")
     * @param Request $request
     * @return Response
     */
    public function showAllAction(Request $request)
    {
        $adRepository = $this->getDoctrine()->getRepository(Ad::class);
        $adsQuery = $adRepository->getAdsQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $adsQuery,
            $request->query->getInt('page', 1),
            self::AD_COUNT_PER_PAGE
        );
        return $this->render('ad/show_all.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit_ad")
     * @param Request $request
     * @param Ad $ad
     * @return Response
     */
    public function editAction(Request $request, Ad $ad)
    {
        if ($this->getUser() !== $ad->getUser()) {
            throw new AccessDeniedException('Only an ad author can edit it!');
        }
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('show_ad', ['id' => $ad->getId()]);
        }
        return $this->render('ad/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit", name="create_ad")
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        if (!$this->getUser()) {
            throw new AccessDeniedException('Only authorized user can create an ad! Please log in!');
        }
        $ad = new Ad();
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $user->addAd($ad);
            $ad->setUser($user)
                ->setAuthorName($user->getUsername());
            $entityManager->persist($ad);
            $entityManager->flush();
            return $this->redirectToRoute('show_ad', ['id' => $ad->getId()]);
        }
        return $this->render('ad/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show_ad")
     * @param Ad $ad
     * @return Response
     */
    public function showAction(Ad $ad)
    {
        return $this->render('ad/show.html.twig', [
            'ad' => $ad
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_ad")
     * @param Ad $ad
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Ad $ad)
    {
        if ($this->getUser() !== $ad->getUser()) {
            throw new AccessDeniedException('Only an ad author can remove it!');
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($ad);
        $entityManager->flush();
        return $this->redirectToRoute('show_ads');
    }
}
