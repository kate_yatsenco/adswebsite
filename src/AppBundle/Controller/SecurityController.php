<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\LoginType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="authorize_user")
     * @param Request $request
     * @Method({"GET", "POST"})
     * @param AuthenticationUtils $authUtils
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function authorizationAction(Request $request, AuthenticationUtils $authUtils, UserPasswordEncoderInterface $passwordEncoder)
    {
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();
        $form = $this->get('form.factory')->createNamed(
            null,
            LoginType::class,
            [
                '_username' => $lastUsername,
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $username = $form["_username"]->getData();
            $plainPassword = $form["_password"]->getData();
            $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
            if (is_null($user)) {
                $newUser = new User();
                $password = $passwordEncoder->encodePassword($newUser, $plainPassword);
                $newUser->setUsername($username);
                $newUser->setPassword($password);
                $entityManager->persist($newUser);
                $entityManager->flush();
                $this->loginUser($request, $newUser);
            } else {
                $isValid = $passwordEncoder->isPasswordValid($user, $plainPassword);
                if ($isValid && !is_null($plainPassword)) {
                    $this->loginUser($request, $user);
                } else {
                    $this->addFlash("error", "Invalid credentials");
                }
            }
            return $this->redirectToRoute('show_ads');
        }
        return $this->render('security/login.html.twig', [
            'username' => $lastUsername,
            'error' => $error,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout",name="logout")
     * @param Request $request
     */
    public function logoutAction(Request $request)
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     * @param Request $request
     * @param User $user
     */
    private function loginUser(Request $request, User $user)
    {
        $token = new UsernamePasswordToken($user, $user->getPassword(), "user", $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }
}
